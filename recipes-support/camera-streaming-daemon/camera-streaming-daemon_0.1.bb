DESCRIPTION = "Camera Streaming Daemon is a software daemon to handle video streaming for drones in general"
DEPENDS = "avahi gstreamer1.0 gstreamer1.0-rtsp-server glib-2.0 python python-future librealsense"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=93888867ace35ffec2c845ea90b2e16b"

SRCREV = "aa675bc0877e5f2331ef301d91eb05b8edaf8223"
SRC_URI = "gitsm://git@gitlab.utc.fr/uav-hds/intel-aero/camera-streaming-daemon.git;protocol=https;branch=master"
SRC_URI += "file://main.conf"

S = "${WORKDIR}/git"

inherit autotools pythonnative pkgconfig systemd

PACKAGECONFIG ??= "${@bb.utils.filter('DISTRO_FEATURES', 'systemd', d)}"
PACKAGECONFIG[systemd] = "--enable-systemd --with-systemdsystemunitdir=${systemd_unitdir}/system/,--disable-systemd"

EXTRA_OECONF_append = " --enable-aero"

do_compile_prepend () {
    export PYTHONPATH="${PKG_CONFIG_SYSROOT_DIR}/usr/lib/python2.7/site-packages/"
}

do_install_append () {
    install -d ${D}${sysconfdir}/dcm/
    install -m 0644 ${WORKDIR}/main.conf ${D}${sysconfdir}/dcm/
}

SYSTEMD_SERVICE_${PN} = "dcm.service"
