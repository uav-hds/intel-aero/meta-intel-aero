#! /bin/sh

### BEGIN INIT INFO
# Provides:          mavlink-routerd
# Required-Start:    
# Required-Stop:     
# Default-Start:     
# Default-Stop:      
# Short-Description: Mavlink routing accross interfaces
# Description:       Routes mavlink packets between interfaces.
### END INIT INFO

. /lib/lsb/init-functions
